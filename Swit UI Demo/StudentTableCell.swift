//
//  StudentTableCell.swift
//  Swit UI Demo
//
//  Created by Minh.Thang on 6/25/19.
//  Copyright © 2019 Hhuyy. All rights reserved.
//

import SwiftUI

struct StudentTableCell : View {
    var student: Student
    var body: some View {
        return HStack {
            ImageStore.shared.image(name: student.urlAvatarString, size: 80)
                .overlay(Circle().stroke(Color.gray, lineWidth: 8))
                .clipShape(Circle())
                .shadow(radius: 8)
            
            VStack(alignment: .leading) {
                Text(student.name)
                HStack {
                    Text("So dien thoai")
                    Spacer()
                    Text(student.phone)
                }
                
            }            
            if student.isFavorite {
            Image(systemName:"star.fill").imageScale(.medium).foregroundColor(.yellow).padding()
            }
        }
    }
}

#if DEBUG
struct StudentTableCell_Previews : PreviewProvider {
    static var previews: some View {
        Group {
            StudentTableCell(student: testStudents[0])
            StudentTableCell(student: testStudents[2])
        }.previewLayout(.fixed(width: 300, height: 70))
        
    }
}
#endif
