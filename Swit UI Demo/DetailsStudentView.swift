//
//  DetailsStudentView.swift
//  Swit UI Demo
//
//  Created by Minh.Thang on 6/23/19.
//  Copyright © 2019 Hhuyy. All rights reserved.
//

import SwiftUI

struct DetailsStudentView : View {
    var student: Student

    var body: some View {
        VStack {
            MapView(locationCoordinate: student.homeLocationCoordinate).frame(height: 300)
            ImageStore.shared.image(name:student.urlAvatarString, size: 200)
            .clipShape(Circle()).overlay(Circle().stroke(Color.gray, lineWidth: 4))
                .offset(y: -100)
                .padding(.bottom, -100)
            .shadow(radius: 4)
            Text(student.name).font(.title)
            Text(student.phone)
            Spacer()
        }
    }
}

#if DEBUG
struct DetailsStudentView_Previews : PreviewProvider {
    static var previews: some View {
        DetailsStudentView(student: testStudents[0])
    }
}
#endif
