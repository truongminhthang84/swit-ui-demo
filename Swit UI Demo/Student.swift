//
//  Student.swift
//  Swit UI Demo
//
//  Created by Minh.Thang on 6/23/19.
//  Copyright © 2019 Hhuyy. All rights reserved.
//

import SwiftUI
import MapKit

struct Student: Identifiable {
    var id = UUID()
    var name : String
    var phone: String
    var urlAvatarString: String
    var homeLocationCoordinate : CLLocationCoordinate2D
    var isFavorite: Bool
}

#if DEBUG

let testStudents = [
    Student(name: "Truong Minh Thang", phone: "0839180123", urlAvatarString: "avatar", homeLocationCoordinate: CLLocationCoordinate2D(latitude: 21.0228161, longitude: 105.8018582), isFavorite: false),
    Student(name: "Truong Minh Thang 1", phone: "0839180123", urlAvatarString: "avatar", homeLocationCoordinate: CLLocationCoordinate2D(latitude: 21.0228161, longitude: 105.8018582), isFavorite: false),
    Student(name: "Truong Minh Thang 2", phone: "0839180123", urlAvatarString: "avatar", homeLocationCoordinate: CLLocationCoordinate2D(latitude: 21.0228161, longitude: 105.8018582), isFavorite: true),
    Student(name: "Truong Minh Thang 3", phone: "0839180123", urlAvatarString: "avatar", homeLocationCoordinate: CLLocationCoordinate2D(latitude: 21.0228161, longitude: 105.8018582), isFavorite: true),
    Student(name: "Truong Minh Thang 4", phone: "0839180123", urlAvatarString: "avatar", homeLocationCoordinate: CLLocationCoordinate2D(latitude: 21.0228161, longitude: 105.8018582), isFavorite: false),
    Student(name: "Truong Minh Thang 5", phone: "0839180123", urlAvatarString: "avatar", homeLocationCoordinate: CLLocationCoordinate2D(latitude: 21.0228161, longitude: 105.8018582), isFavorite: false),
]

#endif
