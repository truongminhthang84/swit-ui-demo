//
//  MapView.swift
//  Swit UI Demo
//
//  Created by Minh.Thang on 6/23/19.
//  Copyright © 2019 Hhuyy. All rights reserved.
//

import SwiftUI
import MapKit

struct MapView : UIViewRepresentable {
    var locationCoordinate : CLLocationCoordinate2D
    typealias UIViewType = MKMapView
    
    func makeUIView(context: UIViewRepresentableContext<MapView>) -> MKMapView {
        MKMapView(frame: .zero)
    }
    
    func updateUIView(_ uiView: MKMapView, context: UIViewRepresentableContext<MapView>) {
        let coordinate = locationCoordinate
        let span = MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02)
        let region = MKCoordinateRegion(center: coordinate, span: span)
        uiView.setRegion(region, animated: true)
    }
   
}

#if DEBUG
struct MapView_Previews : PreviewProvider {
    static var previews: some View {
        MapView(locationCoordinate: CLLocationCoordinate2D(latitude: 21.0228161, longitude: 105.8018582))
    }
}
#endif
