//
//  ImageStore.swift
//  Swit UI Demo
//
//  Created by Minh.Thang on 6/24/19.
//  Copyright © 2019 Hhuyy. All rights reserved.
//

import UIKit
import SwiftUI

final class ImageStore {
    static let shared  = ImageStore()
    private let scale = Int(UIScreen.main.scale)
    var images = Dictionary<String, [Int: CGImage]>()
    private let originSize = -1
    func image(name: String, size: Int) -> Image {
        getOriginImage(name: name)
        if let cgimage = images[name]?[size] {
            return Image(cgimage, scale: Length(scale), label: Text(verbatim: name))
        } else if  let cgimage = images[name]?[originSize]?.resizeImage(size: size * scale) {
           
            images[name] = [size: cgimage]
            return Image(cgimage, scale: Length(scale), label: Text(verbatim: name))
        } else {
            fatalError("Error when get Image with size")
        }
    }
    
    func getOriginImage(name: String) {
        guard let url = Bundle.main.url(forResource: name, withExtension: "jpg"),
            let imageResource = CGImageSourceCreateWithURL(url as CFURL, nil),
            let cgImage = CGImageSourceCreateImageAtIndex(imageResource, 0, nil)
        else {
            fatalError("Error when get origin image")
        }
        images[name] = [originSize: cgImage]
    }
}


// MARK: -

extension CGImage {
    func resizeImage(size: Int) -> CGImage?{
        guard let color = colorSpace, let context = CGContext(data: nil, width: size, height: size, bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: color, bitmapInfo: bitmapInfo.rawValue) else {
            return nil
        }
        context.interpolationQuality = .high
        context.draw(self, in: CGRect(x: 0,y: 0,width: size,height: size))
        
        if let resizedImage = context.makeImage() {
            return resizedImage
        } else {
            fatalError("Resize image unsuccessful")
        }
    }
}
