//
//  ContentView.swift
//  Swit UI Demo
//
//  Created by Minh.Thang on 6/23/19.
//  Copyright © 2019 Hhuyy. All rights reserved.
//

import SwiftUI

struct ContentView : View {
    @State var showOnlyFavorite = false
    var students : [Student] = []
    var body: some View {
        List{
            Toggle(isOn: self.$showOnlyFavorite) {
                Text("Show Only Favorite")
            }
            ForEach(students) { student in
                if (!self.showOnlyFavorite || student.isFavorite) {
                    NavigationButton(destination:
                        DetailsStudentView(student: student)
                    ) {
                        StudentTableCell(student: student)
                    }
                }
                
            }
            
            }.navigationBarTitle(Text("Quan ly sinh vien"), displayMode: .large)
        
    }
}

#if DEBUG
struct ContentView_Previews : PreviewProvider {
    static var previews: some View {
        ForEach(["iPhone SE", "iPhone XS Max", "iPhone 6s"].identified(by: \.self)) { deviceId in
            NavigationView {
                ContentView(students: testStudents)
                }.environment(\.colorScheme, .dark)
                .previewDevice(PreviewDevice(rawValue: deviceId))
            .previewDisplayName(deviceId)
        }
        
        
    }
}
#endif




