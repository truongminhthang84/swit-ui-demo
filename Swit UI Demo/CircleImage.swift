//
//  CircleImage.swift
//  Swit UI Demo
//
//  Created by Minh.Thang on 6/23/19.
//  Copyright © 2019 Hhuyy. All rights reserved.
//

import SwiftUI

struct CircleImage : View {
    var urlString : String
    var body: some View {
        Image(urlString)
                .frame(width: 50, height: 50)
            .aspectRatio(contentMode: .fill)
                .scaledToFit()
.clipShape(Circle())
            
        
    }
}

#if DEBUG
struct CircleImage_Previews : PreviewProvider {
    static var previews: some View {
        CircleImage(urlString: "girl")
    }
}
#endif
